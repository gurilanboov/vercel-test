import Image from 'next/image';
import Link from 'next/link';

const Footer = () => {
    return (
        <footer>
            <Image src="/dummy-pic.jpg" alt="Abstract painting" height={70} width={70} />
            <div className="stuff1">
                <p>Facebook</p>
                <p>Instagram</p>
                <p>Youtube</p>
            </div>
            <div className="stuff2">
                <p>Sneaky Worm</p>
                <p>Terrifying</p>
                <p>Hold off ogres</p>
            </div>
            <div className="stuff3">
                <p>Clients</p>
                <p>Our Magnitude</p>
                <p>Help</p>
            </div>
        </footer>
    );
}
 
export default Footer;