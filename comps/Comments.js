import styles from '../styles/Home.module.css';

const parentheses = "(5)";

const Comments = () => {

    return (     

        <div className="comment">
            <div className={styles.setgegdel}><h2>Сэтгэгдэлүүд { parentheses }</h2></div>
            <div className="setgegdluud">
                <div className="setgegdel1">
                    <div className="icon"></div>
                    <div className="first-comment">
                        <h3 className='first-title'>Nomura</h3>
                        <h3 className="money">10'000₮</h3>
                        <p>Харамсалтай явдал тохиолдсонд сэтгэл шимширч байна. Бага ч болов туслана гэж найдья</p>
                    </div>
                </div>
                <div className="setgegdel1">
                    <div className="icon"></div>
                    <div className="first-comment">
                        <h3 className='first-title'>Barkhas</h3>
                        <h3 className="money">15'000₮</h3>
                        <p>Харамсалтай явдал тохиолдсонд сэтгэл шимширч байна. Бага ч болов туслана гэж найдья</p>
                    </div>
                </div>
                <div className="setgegdel2">
                    <div className="icon"></div>
                    <div className="first-comment">
                        <h3 className='first-title'>Ermuun</h3>
                        <h3 className="money">12'000₮</h3>
                        <p>Харамсалтай явдал тохиолдсонд сэтгэл шимширч байна. Бага ч болов туслана гэж найдья</p>
                    </div>
                </div>
                <div className="setgegdel1">
                    <div className="icon"></div>
                    <div className="first-comment">
                        <h3 className='first-title'>Enh-Saikhan</h3>
                        <h3 className="money">13'000₮</h3>
                        <p>Миний зүрх үнэндээ л өвдөж байна тэхүү.</p>
                    </div>
                </div>
                <div className="setgegdel1">
                    <div className="icon"></div>
                    <div className="first-comment">
                        <h3 className='first-title'>Bilguun-Erkh</h3>
                        <h3 className="money">100'000₮</h3>
                        <p>Ууг нь миний санаа байсан юм даа, за яахав. Хэхэ.</p>
                    </div>
                </div>
            </div>     
            
        </div>
    );
}
 
export default Comments;