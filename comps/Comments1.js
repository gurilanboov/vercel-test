import styles from '../styles/Home.module.css'
import react, {useState} from 'react'

const Comments1 = () => {
    const arrayOfObjects = [
        { coffee: "Americano", size: "Medium" },
        { coffee: "Espresso", size: "Single" },
      ];

    const [title, setTitle] = useState([
        {
            holo: 'Miku-chan',
            gram: 'Meru-chan',
            id: 3
        },
        {
            holo: 'Sara-chan',
            gram: 'Hyra-chan',
            id: 2
        },
        {
            holo: 'Tara-chan',
            gram: 'Kira-chan',
            id: 1
        }
    ])

    console.log(title)

    return (
        <div className="comment1">
            <h2> {title[0].holo }</h2>
            {arrayOfObjects.map(({ coffee, size }) => (
                    <p key={coffee}>Coffee type {coffee} in a {size} size.</p>
                ))}

            {/* {title.map(({ holo}) => (
                <p key={holo}>Holo is {title.holo} and the gram is</p>
            ))} */}

            

        </div>
    );
}
 
export default Comments1;

// import styles from '../styles/Home.module.css';
// import react, { useState } from 'react';
// // import react from 'react';
// // import {useState} from 'react/cjs/react.production.min';
// // import { useState } from "react/cjs/react.development"


// const Comments = () => {
//     // const [blogs, setBlogs] = useState=([
//     //     { author: 'Mario', body: 'I am happy, do not mind me. Why is Peach in different castles all the time. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto quam exercitationem molestiae ducimus nemo reiciendis, modi itaque pariatur officia culpa eveniet esse beatae veritatis soluta voluptate voluptates veniam nesciunt consequuntur.', id: 1},
//     //     { author: 'Luigi', body: 'I am so sick of this. Why is Peach in different castles all the time. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto quam exercitationem molestiae ducimus nemo reiciendis, modi itaque pariatur officia culpa eveniet esse beatae veritatis soluta voluptate voluptates veniam nesciunt consequuntur.', id: 2}
//     // ]);

//     const names = ['jones', 'smith', 'haylee', 'carla', 'timothee'];
//     const arrayOfObjects = [
//         { coffee: "Americano", size: "Medium" },
//         { coffee: "Espresso", size: "Single" },
//       ];

//     return (

        

//         <div className="comment">
//             <div className={styles.setgegdel}><h4>Сэтгэгдэлүүд</h4></div>
//             <div className="setgegdluud">
//                 {/* {blogs.map((blog) => (
//                     <div className="blog-preview" key={blog.id}>
//                         <h2>{ blog.author }</h2>
//                         <p>{ blog.body }</p>
//                     </div>
//                 ))} */}
//                 {/* {blogs.map((blogs) => (

//                 ))} */}
//                 {arrayOfObjects.map(({ coffee, size }) => (
//                     <p key={coffee}>Coffee type {coffee} in a {size} size.</p>
//                 ))}
//             </div>

//             <h2>Setgegdluud</h2>
//             <h2>{blogs.author}</h2>            
            
//         </div>
//     );
// }
 
// export default Comments;
