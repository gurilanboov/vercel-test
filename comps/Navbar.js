import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Image from 'next/image';

const Navbar = () => {
    return (
        <nav>
            <Link href="/"><h1 className={styles.logo}>DusalTusal</h1></Link>
            <select className='dropdown' name='languages' id='lang'>
                <option value="1">Mongolian</option>
                <option value="2">English</option>
            </select>
            <Link href="/Log-in"><a>Нэвтрэх</a></Link>
            <div className="sign-in"><Link href="/Sign-in"><a id='sign-in-burtguuleh'>Бүртгүүлэх</a></Link></div>
        </nav>
    );
}
 
export default Navbar;