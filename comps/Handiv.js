import styles from '../styles/Home.module.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Link from 'next/link'

import {
  faAmbulance,
  faCamera,
  faCalculator
} from "@fortawesome/free-solid-svg-icons"

const Handiv = () => {
    return (
        <div className={styles.handiv}>
          <div id={styles.progress}>
            <div id={styles.progressText}>
              <p>30'000'000₮</p>
              <p>50'000'000₮</p>
            </div>
            <div id={styles.progressBar}>
              <div id={styles.progresssLine}></div>
            </div>
          </div>
          
          <div className={styles.share}>
            <div className="dropSocials">
              <input type="radio" name='socials' id='Facebook'/><label For="Facebook"><FontAwesomeIcon icon={faAmbulance} style={{ fontSize:20, color:"blue"}}/></label>
              <input type="radio" name='socials' id='Instagram'/><label For="Instagram"><FontAwesomeIcon icon={faCalculator} style={{ fontSize:20, color:"blue"}}/></label>
              <input type="radio" name='socials' id='Twitter'/> <label For="Twitter"><FontAwesomeIcon icon={faCamera} style={{ fontSize:20, color:"blue"}}/></label>    
            </div>
          </div>
          <div className="donate">
            <h3>Хандивлах</h3>
          </div>
          <div className="topDonator">
            <h3>Топ Хандивлагчид</h3>
          </div>
          <div className="donators">
            <Link href="/Donators">Бүх Хандивлагчид</Link>
          </div>
        </div>
    );
}
 
export default Handiv;