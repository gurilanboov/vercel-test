import Image from 'next/image'
import styles from '../styles/Home.module.css'

const charityType =['Байгалийн гамшиг', 'Буяны мөнгө', 'Төрийн бус байгууллага', 'Хот тохижилт', 'Сургалтын төлбөр', 'Хувь хүн', 'Хүнд өвчин', '']

const Description = () => {
    return (
        <div className={styles.description}>
          
          <div className={styles.image}><Image className={styles.image} src="/LilThuge2.jpg" alt="flooded house" width={300} height={300} /></div>
          <div className={styles.additionalDesc}>
            <div className={styles.line}></div>
            <h3>3 өдрийн өмнө зарлагдсан</h3>
            <a href="https://en.wikipedia.org/wiki/Leukemia" target="blank">Хандивын төрөл - {charityType[6]}</a>
            <div style={{color: "white"}}>wwwwww</div>
            <div style={{color: "white"}}>wwwwww</div>
            <div className={styles.line}></div>
          </div>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum facere dolore neque eius, quia sit. Impedit a, quis illo sint voluptate maiores amet quisquam, laborum ipsa corrupti itaque magnam tempore!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, repudiandae?</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel modi nobis, dolores illo accusamus quos cupiditate sit delectus vero? Nihil?</p>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus obcaecati possimus reiciendis cupiditate dicta modi fugit excepturi sint ipsa, molestias, enim amet eaque nihil dolorem, laborum maiores reprehenderit in! Aliquid quos quibusdam suscipit harum facere? Porro voluptates odio iste facilis.</p>

          <div className={styles.Allupdates}>
            <h2>Явц</h2>
            <div className={styles.diffLine}></div>
            <div className={styles.updates}>
              <h3>Өнөөдөр</h3>
              <p>"Бүх дэмжиж байгаа хүмүүсийнхээ ачаар бие минь маш их дээрдэж байна. Та бүхэндээ баярлалаа" - гэж хэлээд зогсож байгаа Thuge ах.</p>
              <div className={styles.image}><Image src='/LilBetter.jpg' alt='LilBetter-Thuge' width={120} height={250}/></div>
              <div className={styles.diffLine}></div>
            </div>

            <div className={styles.updates}>
              <h3>5 - Өдрийн өмнө</h3>
              <p>Фундрэйзинг эвентэд хүмүүс ирж оролцов. Өнөөдрийн зочин Ариунаа эгч</p>
              <div className={styles.image}><Image src='/Concert3.jpg' alt='Ariunaa-Egch' width={170} height={250}/></div>
              <div className={styles.diffLine}></div>
            </div>

            <div className={styles.updates}>
              <h3>6 - Өдрийн өмнө</h3>
              <p>Фундрэйзинг эвентэд хүмүүс ирж оролцов. Өнөөдрийн зочин Сарантуяа эгч</p>
              <div className={styles.image}><Image src='/Concert2.jpg' alt='Sarantuya-Egch' width={300} height={250}/></div>
              <div className={styles.diffLine}></div>
            </div>

            <div className={styles.updates}>
              <h3>7 - Өдрийн өмнө</h3>
              <p>Фундрэйзинг эвент эхэллээ!!!</p>
              <div className={styles.image}><Image src='/Concert1.jpg' alt='Sarantuya-Egch' width={180} height={250}/></div>
              <div className={styles.diffLine}></div>
              <div style={{color: "white"}}>wwwwww</div>
            </div>
          </div>
        </div>
    );
}
 
export default Description;