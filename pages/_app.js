import '../styles/globals.css'
import Layout from '../comps/Layout'
import '../comps/Layout'
import "@fortawesome/fontawesome-svg-core/styles.css"
import { config } from "@fortawesome/fontawesome-svg-core"
config.autoAddCss = false;


function MyApp({ Component, pageProps }) {
  // <div>
    return (
      <Layout>
        <Component {...pageProps} />
       </Layout>
    )

   
  {/* </div> */}
  
}

export default MyApp
