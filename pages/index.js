import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Handiv from '../comps/Handiv'
import Description from '../comps/Description'
import Comments from '../comps/Comments'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
// import react, {useState} from 'react'

import {
  faAmbulance,
  faCamera,
  faCalculator
} from "@fortawesome/free-solid-svg-icons"


export default function Home() {
  // const [title, setTitle] = useState('Дэмбээгийхэнд туслацгаая');
  return (
    <div className={styles.body}>
      <div className={styles.spacing}></div>
      <div className={styles.title}><h1> #Save LilThugE </h1></div>
      <div className={styles.content}>
        <Description />
        <Handiv />
      </div>
        <Comments />
    </div>
  )
}

